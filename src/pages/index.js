import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import '../styles/index.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock } from '@fortawesome/free-solid-svg-icons'

const IndexPage = ({data}) => (
	<div>
		<h1> Posts </h1>
		{data.allMarkdownRemark.edges.map(post => (
			<div className='container'>
				<div className='link'>   
					<Img sizes={post.node.frontmatter.image.childImageSharp.sizes} /> 
					<Link 
						key={post.node.id} 
						to={post.node.frontmatter.path}>
						<h3>{post.node.frontmatter.title}</h3>
					</Link>
				</div>
				<div className='date_details'> 
					<p> {post.node.frontmatter.date} <FontAwesomeIcon icon={faClock} /> {post.node.timeToRead} min to read</p>
				</div>
				<p>{post.node.excerpt}</p>
			</div>
		))}
	</div>
)


export const query = graphql`
query indexQuery {
    allMarkdownRemark(limit: 10) {
        edges {
            node {
            excerpt(pruneLength: 250)
            timeToRead
                frontmatter {
                    title
                    path 
                    image {
                      	publicURL
          	            childImageSharp {
                        sizes(maxWidth: 200) {
                        ...GatsbyImageSharpSizes
                      }
                    }
                  }
                    date(formatString: "MMMM DD, YYYY")
                }
            }
        }
    }
}
`



export default IndexPage
