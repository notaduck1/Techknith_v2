module.exports = {
	siteMetadata: {
		title: 'Techknight',
		author: 'Daniel Guldberg Aaes'
	},
	plugins: [
		'gatsby-remark-copy-linked-files',
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp',
		{
			resolve: 'gatsby-remark-images',
			options: {
				maxWidth: 1080,
			}
		},
		'gatsby-plugin-react-helmet',
		'gatsby-plugin-sass',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				path: `${__dirname}/src/pages`,
				name: 'pages',
			}
		},
		'gatsby-transformer-remark',

	],
}
